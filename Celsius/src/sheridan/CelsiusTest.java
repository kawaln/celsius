package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {



	@Test
	public void testIsconvertFromFahrenheit() {
		
		int num = Celsius.convertFromFahrenheit(32);
		assertTrue("Invalid value for Fahrenheit", num == 0);
	
	}

	@Test
	public void testconvertFromFahrenheitException() {
		assertNotEquals("Invalid value for Fahrenheit",
				Celsius.convertFromFahrenheit(3));
	}
	
	@Test
	public void testconvertFromFahrenheitBoundaryIn() {
		int num = Celsius.convertFromFahrenheit(6);
		assertTrue("Invalid value for Fahrenheit", num == -14);
	}

	@Test
	public void testconvertFromFahrenheitBoundaryOut() {
		int num = Celsius.convertFromFahrenheit(7);
		assertFalse("Invalid value for Fahrenheit", num == 14);
	}
}
